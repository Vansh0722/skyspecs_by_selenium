package org.example;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import com.twilio.base.ResourceSet;
import com.twilio.rest.api.v2010.account.Message;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.time.Duration;

public class SkySpecs1 {
    public static final String ACCOUNT_SID = "AC9165c333e78f25dd162008f5196b12b1";
    public static final String AUTH_TOKEN = "d7bcf384a8a30afce56ce75714998f0a";

    static WebDriver driver;

    @Test
    public void invokeBrowser() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.get("https://finance.dev.skyspecs.com/");
    }


    @Test
    public void logInDetails() {
        WebElement logIn = driver.findElement(By.xpath("//a[contains(text(),'Login to Fincovi AM')]"));
        logIn.click();
        WebElement logInEmailAddress = driver.findElement(By.xpath("//input[@id='logonIdentifier']"));
        logInEmailAddress.sendKeys("vanshchaudhary0107@gmail.com");
        WebElement logInPassword = driver.findElement(By.xpath("//input[@id='password']"));
        logInPassword.sendKeys("skySpecs@22");
        WebElement signIn = driver.findElement(By.xpath("//button[@id='next']"));
        signIn.click();
        WebElement sendOtp = driver.findElement(By.xpath("//button[@id='verifyCode']"));
        sendOtp.click();
    }

    public void selectVanshOrgTest() throws InterruptedException {
        Thread.sleep(15000);
        WebElement organisations = driver.findElement(By.linkText("Organisations"));
        organisations.click();
        //SELECT ORG FOR VANSH..
        List<WebElement> vansh = driver.findElements(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/main[1]/div[1]/div[2]/div[1]/div//a"));
        for (WebElement webElement : vansh) {
            if (webElement.getText().equals("TESTORGFORVANSH")) {
                webElement.click();
                break;
            }
        }
    }

    public void createOrganisation()  {
        WebElement createAsset = driver.findElement(By.xpath("//button[@type='button' and text()='Create Asset']"));
        createAsset.click();
    }

    public void companyDetails() {
        //Company Details..
        WebElement projectsClick = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[2]/div[2]/div[1]/div[1]/div[2]"));
        projectsClick.click();
        WebElement formalCompanyName = driver.findElement(By.xpath("//input[@name='formalBusinessName']"));
        formalCompanyName.sendKeys("skySpecs");
        WebElement country = driver.findElement(By.id("react-select-2-input"));
        country.sendKeys("India");
        List<WebElement> countriesList = driver.findElements(By.id("react-select-2-listbox"));
        countriesList.get(0).click();
    }

    public void projectDetails() {
        //project Details..
        String actualDisplayN = driver.findElement(By.xpath("//div[contains(text(),'Display Name')]")).getText();
        WebElement displayName = driver.findElement(By.xpath("//input[@name='displayName']"));
        displayName.sendKeys("TestOrgVansh");
        WebElement dropDownAssertType = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[9]/div[2]/div[1]/div[1]/div[2]/div[1]"));
        dropDownAssertType.click();
        List<WebElement> assetTypes = driver.findElements(By.id("react-select-3-listbox"));
        assetTypes.get(0).click();
        WebElement installedCapacity = driver.findElement(By.xpath("//input[@placeholder='Installed Capacity']"));
        installedCapacity.sendKeys("555");
        WebElement mec = driver.findElement(By.xpath("//input[@placeholder='Maximum Export Capacity (MEC)']"));
        mec.sendKeys("8888");
        String actualDevice = driver.findElement(By.xpath("//div[contains(text(),'No. of Devices')]")).getText();
        WebElement noOfDevices = driver.findElement(By.xpath("//input[@placeholder='No. of Devices']"));
        noOfDevices.sendKeys("1");
        WebElement noOfDevicesSize = driver.findElement(By.xpath("//input[@name='assetDevices.0.size']"));
        noOfDevicesSize.sendKeys("55");
        WebElement deviceSizeMW = driver.findElement(By.xpath("//span[@class='input-group-text' and text()='MW']"));
        deviceSizeMW.click();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)", "");
        WebElement assetPhase = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[16]/div[2]/div[1]/div[1]/div[2]/div[1]"));
        assetPhase.click();
        List<WebElement> assetPhases = driver.findElements(By.id("react-select-4-listbox"));
        assetPhases.get(0).click();
        WebElement assetTimeZone = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[18]/div[2]/div[1]/div[1]/div[1]"));
        assetTimeZone.click();
        List<WebElement> assetTimeZones = driver.findElements(By.id("react-select-5-listbox"));
        assetTimeZones.get(0).click();
    }

    public void financialDetails() {
        //Financial Details..
        WebElement yearEnd = driver.findElement(By.xpath("//div[@class='react-select financialYearEndDate css-b62m3t-container']//div[@class='react-select__indicator react-select__dropdown-indicator css-jm6450-indicatorContainer']"));
        yearEnd.click();
        List<WebElement> yearsEnd = driver.findElements(By.id("react-select-6-listbox"));
        yearsEnd.get(0).click();
        WebElement currency = driver.findElement(By.xpath("//div[@class='react-select currency css-b62m3t-container']//div[@class='react-select__indicator react-select__dropdown-indicator css-jm6450-indicatorContainer']"));
        currency.click();
        List<WebElement> currencies = driver.findElements(By.id("react-select-7-listbox"));
        currencies.get(0).click();
        WebElement ownerShip = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[26]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/input[1]"));
        ownerShip.sendKeys("2");
        WebElement countryForTax = driver.findElement(By.xpath("//div[@class='react-select countryForTax css-b62m3t-container']//div[@class='react-select__indicator react-select__dropdown-indicator css-jm6450-indicatorContainer']"));
        countryForTax.click();
        List<WebElement> countriesForTax = driver.findElements(By.id("react-select-9-listbox"));
        countriesForTax.get(0).click();
        WebElement taxId = driver.findElement(By.xpath("//input[@placeholder='Tax ID']"));
        taxId.sendKeys("12121212121");
        //If you want to create organisation use create instead of cancel
        WebElement cancel = driver.findElement(By.xpath("//button[contains(text(),'Cancel')]"));
        cancel.click();
    }

    //use it when you want to automate otp..
    public static String getMessageOTP() {
        return getMessages().filter(m -> m.getDirection().compareTo(Message.Direction.INBOUND) == 0)
                .filter(m -> m.getTo().equals("+19019796984")).map(Message::getBody).findFirst()
                .orElseThrow(IllegalStateException::new);
    }

    private static Stream<Message> getMessages() {
        ResourceSet<Message> messages = Message.reader(ACCOUNT_SID).read();
        return StreamSupport.stream(messages.spliterator(), false);
    }


    public static void main(String[] args) throws InterruptedException {
        SkySpecs1 fincovi = new SkySpecs1();
        System.setProperty("webdriver.chrome.driver", "/home/vansh/Desktop/java/seleniumProjects/chromedriver");
        fincovi.driver = new ChromeDriver();
        fincovi.invokeBrowser();
        fincovi.logInDetails();
        fincovi.selectVanshOrgTest();
        fincovi.createOrganisation();
        fincovi.companyDetails();
        fincovi.projectDetails();
        fincovi.financialDetails();
    }
}

//  If you are new user then you have to fill these details

//        WebElement countryCode = driver.findElement(By.xpath("//select[@id='countryCode']"));
//        countryCode.click();
//        countryCode.sendKeys("India");
//        countryCode.click();
//        WebElement enterPhoneNumber = driver.findElement(By.xpath("//input[@id='number']"));
//        enterPhoneNumber.sendKeys("9149228469");
//        new WebDriverWait(driver, Duration.ofSeconds(8)).until(
//                ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
//                        "//button[@id='verifyCode']"))));