import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;


public class FincoviTestCases {
    WebDriver driver;


    @Test(priority = 1)
    public void logInPage() {
        String expectedTitle = "Fincovi";
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://finance.dev.skyspecs.com/");
        String actualTitle = driver.getTitle();
        Assert.assertEquals(expectedTitle, actualTitle);
        Assert.assertNotNull(expectedTitle);
    }


    @Test(priority = 2)
    public void logInDetails() throws InterruptedException {
        String expectedTitle = "Sign in";

        WebElement logIn = driver.findElement(By.xpath("//a[contains(text(),'Login to Fincovi AM')]"));
        logIn.click();
        Thread.sleep(8000);
        WebElement logInEmailAddress = driver.findElement(By.xpath("//input[@id='logonIdentifier']"));
        logInEmailAddress.sendKeys("vanshchaudhary0107@gmail.com");
        WebElement logInPassword = driver.findElement(By.xpath("//input[@id='password']"));
        logInPassword.sendKeys("skySpecs@22");
        WebElement signIn = driver.findElement(By.xpath("//button[@id='next']"));
        signIn.click();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle);
        Assert.assertNotNull(actualTitle);
    }

    @Test(priority = 3)
    public void authentication() throws InterruptedException {
        Thread.sleep(5000);
        String expectedTitle = "Multi Factor Authentication";
        WebElement sendOtp = driver.findElement(By.xpath("//button[@id='verifyCode']"));
        sendOtp.click();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle);
        Assert.assertNotNull(actualTitle);
    }


    @Test(priority = 4)
    public void homePgae() throws InterruptedException {

        Thread.sleep(20000);
        String expectedTitle = "Fincovi AM";
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle);
        Assert.assertNotNull(actualTitle);
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(
                ExpectedConditions.visibilityOf(driver.findElement(By.linkText("Organisations"))));
        WebElement organisations = driver.findElement(By.linkText("Organisations"));
        organisations.click();
    }

    @Test(priority = 5)
    public void selectVanshOrgTest() throws InterruptedException {
        Thread.sleep(3000);
        List<WebElement> vansh = driver.findElements(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/main[1]/div[1]/div[2]/div[1]/div//a"));
        for (WebElement webElement : vansh) {
            if (webElement.getText().equals("TESTORGFORVANSH")) {
                String expectedValue = "TESTORGFORVANSH";
                System.out.println(webElement.getText());
                String actualValue = webElement.getText();
                webElement.click();
                Assert.assertEquals(actualValue, expectedValue);
                Assert.assertNotNull(actualValue);
                break;
            }
        }
    }

    @Test(priority = 6)
    public void createOrganisation() throws InterruptedException {
        Thread.sleep(3000);
        String expectedValue = "Create Asset";
        WebElement createAsset = driver.findElement(By.xpath("//button[@type='button' and text()='Create Asset']"));
        String actualValue = createAsset.getText();
        createAsset.click();
        Assert.assertEquals(actualValue, expectedValue);
        Assert.assertNotNull(actualValue);
    }

    @Test(priority = 7)
    public void companyDetails() throws InterruptedException {
        //Company Details..
        Thread.sleep(2000);
        String actualName = driver.findElement(By.xpath("//div[contains(text(),'Formal Company Name')]")).getText();
        Assert.assertEquals(actualName, "Formal Company Name");
        Assert.assertNotNull(actualName);
        WebElement formalCompanyName = driver.findElement(By.xpath("//input[@name='formalBusinessName']"));
        formalCompanyName.sendKeys("skySpecs");
        Thread.sleep(2000);
        String actualCountry = driver.findElement(By.xpath("//label[contains(text(),'Country')]")).getText();
        Assert.assertEquals(actualCountry, "Country");
        Assert.assertNotNull(actualCountry);
        WebElement country = driver.findElement(By.id("react-select-2-input"));
        country.sendKeys("India");
        List<WebElement> countriesList = driver.findElements(By.id("react-select-2-listbox"));
        countriesList.get(0).click();
    }

    @Test(priority = 8)
    public void projectDetails() throws InterruptedException {
        //project Details..
        String actualDisplayN = driver.findElement(By.xpath("//div[contains(text(),'Display Name')]")).getText();
        WebElement displayName = driver.findElement(By.xpath("//input[@name='displayName']"));
        displayName.sendKeys("TestOrgVansh");
        Thread.sleep(2000);
        WebElement dropDownAssertType = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[9]/div[2]/div[1]/div[1]/div[2]/div[1]"));
        dropDownAssertType.click();
        List<WebElement> assetTypes = driver.findElements(By.id("react-select-3-listbox"));
        assetTypes.get(0).click();
        WebElement installedCapacity = driver.findElement(By.xpath("//input[@placeholder='Installed Capacity']"));
        installedCapacity.sendKeys("555");
        WebElement mec = driver.findElement(By.xpath("//input[@placeholder='Maximum Export Capacity (MEC)']"));
        mec.sendKeys("8888");
        String actualDevice = driver.findElement(By.xpath("//div[contains(text(),'No. of Devices')]")).getText();
        WebElement noOfDevices = driver.findElement(By.xpath("//input[@placeholder='No. of Devices']"));
        noOfDevices.sendKeys("1");
        WebElement noOfDevicesSize = driver.findElement(By.xpath("//input[@name='assetDevices.0.size']"));
        noOfDevicesSize.sendKeys("55");
        WebElement deviceSizeMW = driver.findElement(By.xpath("//span[@class='input-group-text' and text()='MW']"));
        deviceSizeMW.click();
        Thread.sleep(2000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)", "");
        WebElement assetPhase = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[16]/div[2]/div[1]/div[1]/div[2]/div[1]"));
        assetPhase.click();
        List<WebElement> assetPhases = driver.findElements(By.id("react-select-4-listbox"));
        assetPhases.get(0).click();
        Thread.sleep(2000);
        WebElement assetTimeZone = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[18]/div[2]/div[1]/div[1]/div[1]"));
        assetTimeZone.click();
        String actualTime = driver.findElement(By.xpath("//div[contains(text(),'Asset Time Zone')]")).getText();
        List<WebElement> assetTimeZones = driver.findElements(By.id("react-select-5-listbox"));
        assetTimeZones.get(0).click();
        Assert.assertEquals(actualDisplayN, "Display Name");
        Assert.assertNotNull(actualDisplayN);
        Assert.assertEquals(actualDevice, "No. of Devices");
        Assert.assertNotNull(actualDevice);
        Assert.assertEquals(actualTime, "Asset Time Zone");
        Assert.assertNotNull(actualTime);
    }


    @Test(priority = 9)
    public void financialDetails() throws InterruptedException {
        //Financial Details..
        Thread.sleep(2000);
        String actualYearEnd = driver.findElement(By.xpath("//div[contains(text(),'Financial Year End')]")).getText();
        WebElement yearEnd = driver.findElement(By.xpath("//div[@class='react-select financialYearEndDate css-b62m3t-container']//div[@class='react-select__indicator react-select__dropdown-indicator css-jm6450-indicatorContainer']"));
        yearEnd.click();
        List<WebElement> yearsEnd = driver.findElements(By.id("react-select-6-listbox"));
        yearsEnd.get(0).click();
        Thread.sleep(2000);
        WebElement currency = driver.findElement(By.xpath("//div[@class='react-select currency css-b62m3t-container']//div[@class='react-select__indicator react-select__dropdown-indicator css-jm6450-indicatorContainer']"));
        currency.click();
        List<WebElement> currencies = driver.findElements(By.id("react-select-7-listbox"));
        currencies.get(0).click();
        WebElement ownerShip = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[26]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/input[1]"));
        ownerShip.sendKeys("2");
        Thread.sleep(2000);
        WebElement countryForTax = driver.findElement(By.xpath("//div[@class='react-select countryForTax css-b62m3t-container']//div[@class='react-select__indicator react-select__dropdown-indicator css-jm6450-indicatorContainer']"));
        countryForTax.click();
        List<WebElement> countriesForTax = driver.findElements(By.id("react-select-9-listbox"));
        countriesForTax.get(0).click();
        String actualTaxId = driver.findElement(By.xpath("//div[contains(text(),'Tax ID')]")).getText();
        WebElement taxId = driver.findElement(By.xpath("//input[@placeholder='Tax ID']"));
        taxId.sendKeys("12121212121");
        String actualButton = driver.findElement(By.xpath("//button[contains(text(),'Cancel')]")).getText();
        WebElement cancel = driver.findElement(By.xpath("//button[contains(text(),'Cancel')]"));
        cancel.click();
        Thread.sleep(2000);
        WebElement emptyOrg = driver.findElement(By.className("empty-card-content"));
        String actualValue2 = emptyOrg.getText();
        String expectedValue2 = "You have no assets to view.";
        Assert.assertEquals(actualYearEnd, "Financial Year End");
        Assert.assertNotNull(actualYearEnd);
        Assert.assertEquals(actualTaxId, "Tax ID");
        Assert.assertNotNull(actualTaxId);
        Assert.assertEquals(actualButton, "Cancel");
        Assert.assertNotNull(actualButton);
        Assert.assertEquals(actualValue2, expectedValue2);
        Assert.assertNotNull(actualValue2);
    }

}
